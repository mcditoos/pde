\documentclass[10pt]{amsart}
\usepackage{geometry}                % See geometry.pdf to learn the layout options. There are lots.
\geometry{a4paper}                   % ... or a4paper or a5paper or ... 
%\geometry{landscape}                % Activate for for rotated page geometry
%\usepackage[parfill]{parskip}    % Activate to begin paragraphs with an empty line rather than an indent
\usepackage{graphicx}
\usepackage{amssymb}
\usepackage{epstopdf}
\DeclareGraphicsRule{.tif}{png}{.png}{`convert #1 `dirname #1`/`basename #1 .tif`.png}

\title{Energy Estimates}
\author{Oscar Reula}
%\date{}                                           % Activate to display a given date or no date

\begin{document}
\maketitle
%\section{}
%\subsection{}

\section{Introduction}

Central to the proof of existence, uniqueness and continuity w.r.t. initial data of hyperbolic equations are the so called energy estimates. 
They are referred  to as \textsl{a priori} estimates, for the assume a solution exists for every smooth initial data set. 
They consists in defining an appropriate norm, called the \textsl{energy norm} for solutions at a given, arbitrary time, and showing that its time derivative is bounded by a function of itself. That is, 

\[
\frac{d \mathcal{E}}{dt} \leq \mathcal{F}(\mathcal{E}).
\]
Furthermore, $\mathcal{F}$ is differentiable, and $\mathcal{F}(0) = 0$.
It is not difficult to see that if $\mathcal{Y}(t)$ is a solution to 

\[
\frac{d \mathcal{Y}}{dt} \leq \mathcal{F}(\mathcal{Y})\;\;\;\; \mbox{with}\;\; \mathcal{Y}(0) = \mathcal{E}(0),
\]
%
then, 

\[
mathcal{E}(t) = mathcal{Y}(t)
\]
on the whole interval of existence of $mathcal{Y}(t)$, and so there is a finite time interval where the energy, namely the norm of the solution is bounded. The properties of $\mathcal{F}$ imply that solutions are smooth functions of initial data sets in the energy norm.

We start with a couple of representative examples.

\section{The Advection Equation}


We consider first the simplest case, the advection equation in the circle:

\[
\partial_t u = a \partial_x u, \;\;\;\; u(t=0,x) = f(x)
\]
%
where $a$ is a constant. 
We define the \textsl{energy},

\[
\mathcal{E}(t) := \int_{S^1} |u(t,\cdot)|^2 \;dx 
\]

Taking a time derivative we get, 

\begin{eqnarray*}
\frac{d\mathcal{E}(t)}{dt} &=& \int_{S^1} [ \bar{u} \partial_t u + \partial_t \bar{u} u ]\;  dx \\
                                        &=& \int_{S^1} [ \bar{u} a \partial_x u + \bar{a} \partial_x \bar{u} u ]\;  dx \\
                                        &=& \int_{S^1} [ a \partial_x (\bar{u} u) + (\bar{a} - a) u \partial_x \bar{u}  ]\;  dx \\
                                        &=& \int_{S^1} (\bar{a} - a) u \partial_x \bar{u}  \;  dx \\
\end{eqnarray*}
% 
where in the last line we have integrated away the total derivative. Thus, if $a$ is real we have that the energy is conserved. Otherwise, the last term can not be integrated away and the energy depends on derivative of the fields. In that case we know there is no continuity w.r.t. the initial data: we can make a Cauchy sequence of smooth function ion the norm $L^2$, and even in the norm $C^0$, but when taken as initial data, the corresponding solutions grow without bound for any $t\neq 0$. Energy conservation implies that 

\[
||u(t)||^2_{L^2} = \mathcal{E}(t) =  \mathcal{E}(0) = ||u(0)||^2_{L^2} = ||f ||^2_{L^2}.
\]
Thus, in the $L^2$ norm the solution at any given time is a continuous function of the initial data \textsl{in the same norm}. 

Since $u^{(1)} := \partial_x u$ satisfies the same equation, $\partial_t u^{(1)} = a \partial_x u^{(1)}$ we also have, 

\[
||u^{(1)}(t)||^2_{L^2} = ||u^{(1)}(0)||^2_{L^2} = ||\partial_x f ||^2_{L^2}.
\]

In the same way we see that if the data is in the Sobolev space $H^m$, 
\[
||\partial^m_x u(t,\cdot)||^2_{L^2} = ||u^{(m)}(t)||^2_{L^2} = ||u^{(m)}(0)||^2_{L^2} = ||\partial^m_x f ||^2_{L^2},
\]
%
then the solution at any given time is also in that space and we have continuity on any Sobolev space for which the data belongs to. Notice that in all the procedure we have been doing operations, like taking derivatives, and multiplying different functions and integrating by parts as if they were smooth. This is indeed what we have assumed, but then, after finishing the estimate, we can take Cauchy sequences in both spaces (assuming a smooth solution exists for each smooth initial data in the sequence) and then taking the limit, which are guaranty to exists because the density of smooth functions and the the completeness of spaces.



\section{Burgers Equations}


We consider Burgers equation, again in the circle:

\[
\partial_t u = u \partial_x u, \;\;\;\; u(t=0,x) = f(x) \;\; \mbox{a real function}
\]
% 
We define the \textsl{energy},

\[
\mathcal{E}(t) := \int_{S^1} u(t,\cdot)^2 \;dx 
\]

Taking a time derivative we get, 

\begin{eqnarray*}
\frac{d\mathcal{E}_0(t)}{dt} &=& \int_{S^1} 2 u \partial_t u \;  dx \\
                                        &=& \int_{S^1} 2 u^2 \partial_x u \;    dx \\
                                        &=& \int_{S^1} \frac{2}{3} \partial_x(u^3)\;  dx \\
                                        &=& 0
\end{eqnarray*}
% 
Note that we could have used any positive power equal or grater than $1$ and gotten the same conservation.
This equation has an infinite number of conserved quantities! Nevertheless we know that smooth solutions exist only for a limited time span! So, even when these quantities seems to be preserved for an infinite time, the solution nevertheless only lives and is unique for a short time.

The finite differentiability appears as soon as we consider norms for derivatives, defining again $v := \partial_x u$, this time we get, 

\[
\partial_t v = (u \partial_x u)_x = v^2 + u \partial_x v,
\]
%
and,

\[
\mathcal{E}_1(t) := \int_{S^1} v(t,\cdot)^2  \;dx 
\]

\begin{eqnarray*}
\frac{d\mathcal{E}_1(t)}{dt} &=& \int_{S^1} 2 v \partial_t v \;  dx \\
                                        &=& \int_{S^1} 2 [v^3 +  v u \partial_x v] \;    dx \\
                                        &=& \int_{S^1} v^3 \;  dx \\
                                        &=& \sup_{x\in S^1}\{|v|\} \int_{S^1} v^2 \;  dx \\
                                        &\leq& \sup_{x\in S^1}\{|\partial_x u |\} \mathcal{E}_1(t)
\end{eqnarray*}
% 

So we still do not have an estimate with the properties we need, in fact, the equation for $v$ says that,
integrating along the vector $l^a = (1,u)$ from any point, $x_0$, at the initial surface we will be solving,

\[
\frac{d}{d\lambda} v = v^2
\]
with has as a solution, 

\[
v(t) = \frac{v_0}{\lambda v - 1},
\]
where $v_0$ is the value of $v=\partial_x u$ at $(t=0,x_0)$. 
So, if $v_0> 0$ the solution blows up at $\lambda = \frac{1}{v_0}$ (if not to the past). 
Notice that the existence time is bounded by the maximum of the space derivative of $u$, 
\[
|\lambda_{min}| = \frac{1}{v_{max}}, \;\;\;\;\;\; \mbox{with} \;\;\;\;\;\; v_{max} := \sup_{x \in S^1}\{|\partial_x u |\}
\]

To get the desired estimate we need to control also the maximum of the derivative while the solution is smooth.
To do that we consider another space derivative, $w := \partial^2_x u = \partial_x v$.

this time we get, 

\[
\partial_t w = (v^2 + u \partial_x v)_x =  3 v w  + u \partial_x w,
\]
%
and,

\[
\mathcal{E}_2(t) := \int_{S^1} w(t,\cdot)^2  \;dx 
\]

\begin{eqnarray*}
\frac{d\mathcal{E}_2(t)}{dt} &=& \int_{S^1} 2 w \partial_t w \;  dx \\
                                        &=& \int_{S^1} 2w [3 v w  + u \partial_x w] \;    dx \\
                                        &=& \int_{S^1} [6v w^2 + 2u w \partial_x w] \;  dx \\
                                        &=& \int_{S^1} [6v w^2 + u \partial_x w^2] \;  dx \\
                                        &=& \int_{S^1} [6v w^2 -  v w^2 + \partial_x (v w^2)] \;  dx \\
                                        &=& 5 \sup_{x\in S^1}\{|v|\} \int_{S^1} w^2 \;  dx \\
                                        &\leq& \sup_{x\in S^1}\{|\partial_x u |\} \mathcal{E}_2(t).
\end{eqnarray*}
% 
So, again we get the same condition on the point-wise growth of the first derivative. But this time we can use Sobolev lemma which asserts there exists a constant $C_s$ such that,

\[
\sup_{x\in S^1}\{|\partial_x u |\} \leq C_s ||u||_{H^2}.
\]
But the sum of the three energies is just $||u||^2_{H^2}$, thus, summing the estimates we get, 

\[
||u(t)||^2_{H^2} \leq 5 C_s (||u||_{H^2})^{3/2}.
\]

And this really is an energy estimate.


\section{Symmetric Hyperbolic Systems}


























\end{document}